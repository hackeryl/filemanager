﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FileManager
{
    public enum NodeType : int
    {
        File = 0,
        Directory = 1
    }

    public class Node
    {
        public NodeType NodeType { get; set; }
        public Dictionary<string, Node> Directories { get; set; }
        public Dictionary<string, Node> Files { get; set; }
        public string Name { get; set; }
        public byte[] Content { get; set; }
        
        public Node()
        {
            Directories = new Dictionary<string, Node>();
            Files = new Dictionary<string, Node>();
        }
        
    }
}
