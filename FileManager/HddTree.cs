﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileManager
{
    public class HddTree
    {
        public HddTree()
        {

        }

        public TreeNode Root
        {
            get
            {
                return GetStructure();
            }
        }        

        public TreeNode GetDirectoryTree(DirectoryInfo directoryInfo, int depth = 1)
        {
            var root = new TreeNode(directoryInfo.Name, 0, 0)
            {
                Tag = directoryInfo
            };

            if (depth > 0)
            {
                DirectoryInfo[] directories = null;
                try
                {
                    directories = directoryInfo.GetDirectories();
                }
                catch (Exception)
                {
                }
                if (directories != null)
                {
                    foreach (var directory in directories)
                    {
                        root.Nodes.Add(GetDirectoryTree(directory, depth - 1));
                    }
                }
                FileInfo[] files = null;
                try
                {
                    files = directoryInfo.GetFiles();
                }
                catch (Exception)
                {

                }
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        root.Nodes.Add(new TreeNode(file.Name, 1, 1) { Tag = file });
                    }
                }
            }            
            return root;
        }

        public TreeNode[] GetSubDirectoryTree(DirectoryInfo directoryInfo, int depth = 1)
        {
            List<TreeNode> result = new List<TreeNode>();
            if (depth > 0)
            {
                DirectoryInfo[] directories = null;
                try
                {
                    directories = directoryInfo.GetDirectories();
                }
                catch (Exception)
                {
                }
                if (directories != null)
                {
                    foreach (var directory in directories)
                    {
                        result.Add(GetDirectoryTree(directory, depth));
                    }
                }
                FileInfo[] files = null;
                try
                {
                    files = directoryInfo.GetFiles();
                }
                catch (Exception)
                {

                }
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        result.Add(new TreeNode(file.Name, 1, 1) { Tag = file });
                    }
                }
            }
            return result.ToArray();
        }

        private TreeNode GetStructure()
        {
            TreeNode root = new TreeNode("My Computer");
            string[] drives = Environment.GetLogicalDrives();
            foreach (var drive in drives)
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(drive);
                TreeNode driveNode = GetDirectoryTree(directoryInfo, 2);
                root.Nodes.Add(driveNode);
            }
            return root;
        }
    }
}
