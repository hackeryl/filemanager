﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager
{
    public class Trie
    {
        bool isFile = false;
        bool isDirectory = false;
        byte[] content;
        Trie[] fii;

        public Trie()
        {
            fii = new Trie[100];
        }

        private static int getInt(ref int start, byte[] fileContent)
        {

            int first_octet = fileContent[start];
            int second_octet = fileContent[start + 1];
            int third_octet = fileContent[start + 2];
            int forth_octet = fileContent[start + 3];

            start = start + 4;

            return forth_octet + (third_octet << 8) + (second_octet << 16) + (first_octet << 24);
        }//deserializare

        private static byte getByte(ref int start, byte[] fileContent)
        {
            start += 1;
            return fileContent[start - 1];
        }//deserializare

        public static Trie Deserialize(byte[] fileContent)
        {
            int n;
            int current_offset = 0;

            n = getInt(ref current_offset, fileContent);

            Trie[] myNodes = new Trie[n + 1];

            myNodes[0] = new Trie();

            for (int i = 1; i <= n; ++i)
            {
                int tata = getInt(ref current_offset, fileContent);
                byte litera = getByte(ref current_offset, fileContent);
                byte file_or_dir = getByte(ref current_offset, fileContent);

                Trie nod = new Trie();
                if ((int)file_or_dir == 0) nod.isDirectory = true;
                else if ((int)file_or_dir == 1) nod.isFile = true;

                if (nod.isFile)
                {
                    int data_offset = getInt(ref current_offset, fileContent);
                    int data_dimenssion = getInt(ref current_offset, fileContent);

                    nod.content = new byte[data_dimenssion];
                    for (int j = 0; j < data_dimenssion; ++j) nod.content[j] = fileContent[j + data_offset];
                }

                myNodes[i] = nod;

                if ((int)litera >= 32 && (int)litera <= 127)
                    myNodes[tata].fii[(int)litera - 32] = nod;

            }

            return myNodes[1];
        }//deserializare

        private static void AddInt(int value, ref List<byte> content)
        {
            int first_byte = (value >> 24);
            int second_byte = ((value << 8) >> 24);
            int third_byte = ((value << 16) >> 24);
            int fourth_byte = ((value << 24) >> 24);

            content.Add((byte)first_byte);
            content.Add((byte)second_byte);
            content.Add((byte)third_byte);
            content.Add((byte)fourth_byte);

        }//serializare

        private static void AddByte(byte value, ref List<byte> content)
        {
            content.Add(value);
        }//serializare

        private static int GetMetadataLength(Trie root)
        {
            int length = 0;
            List<Trie> coada = new List<Trie>();
            int st = 0, en = 0;
            coada.Add(root);

            while (st <= en)
            {
                Trie nodc = coada[st++];

                //adaug informatii despre nod
                // adaug tatal pe 4 octeti
                length += 4;

                //adaug litera pe 1 octeti
                length += 1;

                //adaug daca e fisier sau nu pe 1 octet
                length += 1;

                if (nodc.isFile)
                {
                    //daca este file ii mai scriu data_offset-ul si dimensiunea pe 8 octeti
                    length += 8;
                }

                //adaug fii in coada

                for (int i = 0; i < 100; ++i)
                    if (nodc.fii[i] != null)
                    {

                        coada.Add(nodc.fii[i]);
                        ++en;

                    }

            }

            return length;
        }//serializare

        public static List<byte> Serialize(Trie root)
        {
            List<byte> content = new List<byte>();

            List<Trie> coada = new List<Trie>();
            List<byte> datalist = new List<Byte>();
            List<int> tati = new List<int>();
            List<byte> litere = new List<byte>();

            int st = 0, en = 0;

            coada.Add(root);
            tati.Add(0);
            litere.Add(32);

            AddInt(0, ref content);

            int data_offset = GetMetadataLength(root) + 4, nr = 0;

            while (st <= en)
            {
                Trie nodc = coada[st];
                int tata = tati[st];
                byte litera = litere[st++];
                ++nr;

                //adaug informatii despre nod
                AddInt(tata, ref content);
                AddByte(litera, ref content);

                if (nodc.isFile)
                {
                    AddByte(1, ref content);
                }
                else if (nodc.isDirectory)
                {
                    AddByte(0, ref content);
                }
                else
                {
                    AddByte(2, ref content);
                }

                if (nodc.isFile)
                {
                    AddInt(data_offset, ref content);
                    AddInt(nodc.content.Length, ref content);

                    for (int i = 0; i < nodc.content.Length; ++i)
                    {
                        datalist.Add(nodc.content[i]);
                        ++data_offset;
                    }
                }

                //adaug fii in coada

                for (int i = 0; i < 100; ++i)
                    if (nodc.fii[i] != null)
                    {

                        coada.Add(nodc.fii[i]);
                        tati.Add(st);
                        litere.Add((byte)(i + 32));
                        ++en;

                    }

            }

            //adaug la inceput numarul de noduri
            int first_byte = (nr >> 24);
            int second_byte = ((nr << 8) >> 24);
            int third_byte = ((nr << 16) >> 24);
            int fourth_byte = ((nr << 24) >> 24);

            content[0] = ((byte)first_byte);
            content[1] = ((byte)second_byte);
            content[2] = ((byte)third_byte);
            content[3] = ((byte)fourth_byte);
            //--------------------

            //---adaug informatia din datalist
            content.AddRange(datalist);

            return content;
        }//serializare

        public static void CreateFile(ref Trie root, String filePath)
        {//daca fisierul deja exista ramine neschimbat
            Trie aux = root;

            for (int i = 0; i < filePath.Length; ++i)
            {
                if (aux.fii[(int)filePath[i] - 32] == null)
                {
                    aux.fii[(int)filePath[i] - 32] = new Trie();
                    aux = aux.fii[(int)filePath[i] - 32];
                }
                else aux = aux.fii[(int)filePath[i] - 32];

            }

            aux.isFile = true;
        }

        public static void CreateDirectory(ref Trie root, String dirPath)
        { // daca directorul deja exista ramine neschimbat
            Trie aux = root;

            for (int i = 0; i < dirPath.Length; ++i)
            {
                if (aux.fii[(int)dirPath[i] - 32] == null)
                {
                    aux.fii[(int)dirPath[i] - 32] = new Trie();
                    aux = aux.fii[(int)dirPath[i] - 32];
                }
                else aux = aux.fii[(int)dirPath[i] - 32];

            }

            aux.isDirectory = true;
        }

        public static void RemoveNod(ref Trie root, String path)
        {  //daca nodul nu exista nu se intimpla nimic
            Trie aux = root;

            for (int i = 0; i < path.Length - 1; ++i)
            {
                if (aux.fii[(int)path[i] - 32] == null)
                {
                    return;
                }
                else aux = aux.fii[(int)path[i] - 32];

            }

            if (aux.fii[(int)path[path.Length - 1] - 32] == null) return;
            else
                if (aux.fii[(int)path[path.Length - 1] - 32].isFile || aux.fii[(int)path[path.Length - 1] - 32].isDirectory)
                {
                    aux.fii[(int)path[path.Length - 1] - 32] = null;
                }

        }

        public static void Rename_Or_Move(ref Trie root, String path1, String path2)
        {//caut mai intii nodul ce descrie folderul sau fisierul de la path1
            //apoi caut si ma asigur ca nu exista path2
            //daca path2 nu exista adaug ultimul nod din path2 ca fiind nodul gasit la 1
            //daca insa path2 exista nu se intimpla nimic

            Trie aux = root;
            bool ok = false;

            for (int i = 0; i < path2.Length - 1; ++i)
            {
                if (aux.fii[(int)path2[i] - 32] == null)
                {
                    ok = true;
                    aux.fii[(int)path2[i] - 32] = new Trie();
                    aux = aux.fii[(int)path2[i] - 32];
                }
                else aux = aux.fii[(int)path2[i] - 32];
            }

            if (aux.fii[(int)path2[path2.Length - 1] - 32] == null) ok = true;

            if (ok == false) return;

            //path2 nu exista

            Trie aux1 = root;

            for (int i = 0; i < path1.Length - 1; ++i)
            {
                if (aux1.fii[(int)path1[i] - 32] == null) return;
                else aux1 = aux1.fii[(int)path1[i] - 32];
            }

            //se pare ca exista path1 dar mai trebuie sa verific ultimul nod + verific daca este direcotr sau fisier
            if (aux1.fii[(int)path1[path1.Length - 1] - 32] == null) return;
            else
                if (aux1.fii[(int)path1[path1.Length - 1] - 32].isFile || aux1.fii[(int)path1[path1.Length - 1] - 32].isDirectory)
                {
                    aux.fii[(int)path2[path2.Length - 1] - 32] = aux1.fii[(int)path1[path1.Length - 1] - 32];
                    aux1.fii[(int)path1[path1.Length - 1] - 32] = null;
                }

        }

        public static void Modify(ref Trie root, String path, String newContent)
        {//caut nodul de la calea rspectiva apoi verific daca e fisier daca da modific continutul
            // daca fisierul nu exista nu se intimpla nimic

            Trie aux = root;

            for (int i = 0; i < path.Length; ++i)
            {

                if (aux.fii[(int)path[i] - 32] == null) return;
                else aux = aux.fii[(int)path[i] - 32];

            }

            if (aux.isFile)
            {
                aux.content = new byte[newContent.Length];
                for (int i = 0; i < newContent.Length; ++i) aux.content[i] = (byte)newContent[i];
            }

        }

        public static String GetContent(Trie root, String path)
        {
            String rez = "";

            Trie aux = root;
            for (int i = 0; i < path.Length; ++i)
            {
                if (aux.fii[(int)path[i] - 32] == null) return rez;
                else aux = aux.fii[(int)path[i] - 32];
            }

            if (aux.isFile)
            {

                //for (int i = 0; i < aux.content.Length; ++i) rez += aux.content[i];
                rez = System.Text.Encoding.UTF8.GetString(aux.content);

            }

            return rez;
        }
    }


    public class FileManagerTrie
    {

        public static void test()
        {
            //creez o mini structura cu un director si doua fisiere in care scriu ceva text
            Trie root = new Trie();
            Trie.CreateDirectory(ref root, "d");
            Trie.CreateFile(ref root, "d/f");
            Trie.CreateFile(ref root, "d/g");
            Trie.Modify(ref root, "d/f", "Hello World 1");
            Trie.Modify(ref root, "d/g", "Hello World 2");

            //serializez arborele curent

            List<byte> ser = Trie.Serialize(root);

            byte[] serByteArray = new byte[ser.Count];
            for (int i = 0; i < ser.Count; ++i)
            {
                serByteArray[i] = ser[i];
            }

            System.IO.File.WriteAllBytes(@"E:\sample.txt", serByteArray);

            Trie auxRoot = Trie.Deserialize(serByteArray);

            //dupa ce am in auxRoot arborele deserializat verific integritatea datelor

            String hello1 = Trie.GetContent(auxRoot, "d/f");
            String hello2 = Trie.GetContent(auxRoot, "d/g");

            Trie.RemoveNod(ref auxRoot, "d/f");

            String hello3 = Trie.GetContent(auxRoot, "d/f");

            Trie.Rename_Or_Move(ref auxRoot, "d/g", "d/f");

            hello3 = Trie.GetContent(auxRoot, "d/f");

            Trie.RemoveNod(ref auxRoot, "d/f");

            hello3 = Trie.GetContent(auxRoot, "d/f");

            ser = Trie.Serialize(auxRoot);

            serByteArray = new byte[ser.Count];
            for (int i = 0; i < ser.Count; ++i)
            {
                serByteArray[i] = ser[i];
            }

            System.IO.File.WriteAllBytes(@"E:\sample2.txt", serByteArray);

        }

    }
}
