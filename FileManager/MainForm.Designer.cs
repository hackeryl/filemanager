﻿namespace FileManager
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.hddTreeView = new System.Windows.Forms.TreeView();
            this.fileManagerTreeView = new System.Windows.Forms.TreeView();
            this.lblHdd = new System.Windows.Forms.Label();
            this.lblFileManager = new System.Windows.Forms.Label();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.btnSaveFile = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tbSearchFile = new System.Windows.Forms.TextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnCreateFolder = new System.Windows.Forms.Button();
            this.btnNewFile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // hddTreeView
            // 
            this.hddTreeView.AllowDrop = true;
            this.hddTreeView.LabelEdit = true;
            this.hddTreeView.Location = new System.Drawing.Point(13, 32);
            this.hddTreeView.Name = "hddTreeView";
            this.hddTreeView.Size = new System.Drawing.Size(470, 425);
            this.hddTreeView.TabIndex = 0;
            this.hddTreeView.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.hddTreeView_AfterExpand);
            this.hddTreeView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.treeViewItemDrag);
            this.hddTreeView.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.hddTreeView_NodeMouseDoubleClick);
            this.hddTreeView.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeViewDragDrop);
            this.hddTreeView.DragEnter += new System.Windows.Forms.DragEventHandler(this.treeViewDragEnter);
            // 
            // fileManagerTreeView
            // 
            this.fileManagerTreeView.AllowDrop = true;
            this.fileManagerTreeView.LabelEdit = true;
            this.fileManagerTreeView.Location = new System.Drawing.Point(526, 32);
            this.fileManagerTreeView.Name = "fileManagerTreeView";
            this.fileManagerTreeView.Size = new System.Drawing.Size(470, 425);
            this.fileManagerTreeView.TabIndex = 1;
            this.fileManagerTreeView.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.fileManagerTreeView_AfterLabelEdit);
            this.fileManagerTreeView.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.fileManagerTreeView_AfterExpand);
            this.fileManagerTreeView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.treeViewItemDrag);
            this.fileManagerTreeView.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeViewDragDrop);
            this.fileManagerTreeView.DragEnter += new System.Windows.Forms.DragEventHandler(this.treeViewDragEnter);
            // 
            // lblHdd
            // 
            this.lblHdd.AutoSize = true;
            this.lblHdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHdd.Location = new System.Drawing.Point(16, 4);
            this.lblHdd.Name = "lblHdd";
            this.lblHdd.Size = new System.Drawing.Size(57, 25);
            this.lblHdd.TabIndex = 2;
            this.lblHdd.Text = "HDD";
            // 
            // lblFileManager
            // 
            this.lblFileManager.AutoSize = true;
            this.lblFileManager.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileManager.Location = new System.Drawing.Point(522, 4);
            this.lblFileManager.Name = "lblFileManager";
            this.lblFileManager.Size = new System.Drawing.Size(138, 25);
            this.lblFileManager.TabIndex = 3;
            this.lblFileManager.Text = "File Manager";
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Location = new System.Drawing.Point(664, 3);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(48, 23);
            this.btnOpenFile.TabIndex = 4;
            this.btnOpenFile.Text = "Open";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // btnSaveFile
            // 
            this.btnSaveFile.Location = new System.Drawing.Point(718, 3);
            this.btnSaveFile.Name = "btnSaveFile";
            this.btnSaveFile.Size = new System.Drawing.Size(48, 23);
            this.btnSaveFile.TabIndex = 5;
            this.btnSaveFile.Text = "Save";
            this.btnSaveFile.UseVisualStyleBackColor = true;
            this.btnSaveFile.Click += new System.EventHandler(this.btnSaveFile_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(947, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(49, 23);
            this.btnSearch.TabIndex = 6;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tbSearchFile
            // 
            this.tbSearchFile.Location = new System.Drawing.Point(795, 5);
            this.tbSearchFile.Name = "tbSearchFile";
            this.tbSearchFile.Size = new System.Drawing.Size(146, 20);
            this.tbSearchFile.TabIndex = 7;
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.Red;
            this.btnDelete.Location = new System.Drawing.Point(489, 66);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(31, 28);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "X";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnCreateFolder
            // 
            this.btnCreateFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateFolder.ForeColor = System.Drawing.Color.Red;
            this.btnCreateFolder.Location = new System.Drawing.Point(489, 32);
            this.btnCreateFolder.Name = "btnCreateFolder";
            this.btnCreateFolder.Size = new System.Drawing.Size(31, 28);
            this.btnCreateFolder.TabIndex = 9;
            this.btnCreateFolder.Text = "+";
            this.btnCreateFolder.UseVisualStyleBackColor = true;
            this.btnCreateFolder.Click += new System.EventHandler(this.btnCreateFolder_Click);
            // 
            // btnNewFile
            // 
            this.btnNewFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewFile.ForeColor = System.Drawing.Color.Red;
            this.btnNewFile.Location = new System.Drawing.Point(489, 100);
            this.btnNewFile.Name = "btnNewFile";
            this.btnNewFile.Size = new System.Drawing.Size(31, 28);
            this.btnNewFile.TabIndex = 10;
            this.btnNewFile.Text = "F";
            this.btnNewFile.UseVisualStyleBackColor = true;
            this.btnNewFile.Click += new System.EventHandler(this.btnNewFile_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 469);
            this.Controls.Add(this.btnNewFile);
            this.Controls.Add(this.btnCreateFolder);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.tbSearchFile);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnSaveFile);
            this.Controls.Add(this.btnOpenFile);
            this.Controls.Add(this.lblFileManager);
            this.Controls.Add(this.lblHdd);
            this.Controls.Add(this.fileManagerTreeView);
            this.Controls.Add(this.hddTreeView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "File Manager";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView hddTreeView;
        private System.Windows.Forms.TreeView fileManagerTreeView;
        private System.Windows.Forms.Label lblHdd;
        private System.Windows.Forms.Label lblFileManager;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Button btnSaveFile;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox tbSearchFile;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnCreateFolder;
        private System.Windows.Forms.Button btnNewFile;
    }
}

