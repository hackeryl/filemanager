﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileManager
{
    public partial class MainForm : Form
    {
        private HddTree hddTree = new HddTree();
        private FileManagerTree fileManagerTree;
        private string dataFilePath;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            InitImageList();
            InitHddTreeView();
            InitFileManagerTreeView();
        }

        private void InitImageList()
        {
            ImageList imageList = new ImageList();
            imageList.Images.Add(FileManager.Properties.Resources.folder);
            imageList.Images.Add(FileManager.Properties.Resources.file);
            hddTreeView.ImageList = imageList;
            fileManagerTreeView.ImageList = imageList;
        }

        private void InitFileManagerTreeView()
        {
        }

        #region HddTree

        private void InitHddTreeView()
        {
            var root = hddTree.Root;
            hddTreeView.Nodes.Add(root);
            root.Expand();
        }

        private void hddTreeView_AfterExpand(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag is DirectoryInfo)
            {
                foreach (TreeNode node in e.Node.Nodes)
                {
                    if (node.Nodes.Count == 0 && node.Tag is DirectoryInfo)
                    {
                        node.Nodes.AddRange(hddTree.GetSubDirectoryTree(node.Tag as DirectoryInfo));
                    }
                }
            }
        }

        private void hddTreeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag is FileInfo)
            {
                ShowFileContent(e.Node.Tag as FileInfo);
            }
        }

        private void ShowFileContent(FileInfo fileInfo)
        {
            Process.Start(fileInfo.FullName);
        }

        #endregion

        #region FileManagerTree

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog.Filter = "Data Files (.data)|*.data";
            openFileDialog.FilterIndex = 0;

            openFileDialog.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            var dialogResult = openFileDialog.ShowDialog();

            // Process input if the user clicked OK.
            if (dialogResult == DialogResult.OK)
            {
                if (File.Exists(openFileDialog.FileName))
                {
                    dataFilePath = openFileDialog.FileName;
                    var data = File.ReadAllBytes(dataFilePath);
                    InitFileManagerTree(data);
                }
            }
        }

        private void InitFileManagerTree(byte[] data)
        {
            AES aes = new AES();
            var content = aes.Decrypt(data);
            fileManagerTree = FileManagerTree.Deserialize(content);
            PopulateFileManagerTree();
        }

        private void PopulateFileManagerTree()
        {
            fileManagerTreeView.Nodes.Clear();
            var root = fileManagerTree.Root;
            var node = fileManagerTree.GetDirectoryTree(root);
            fileManagerTreeView.Nodes.Add(node);
            node.Expand();
        }

        private void btnSaveFile_Click(object sender, EventArgs e)
        {
            AES aes = new AES();
            var data = aes.Encrypt(FileManagerTree.Serialize(fileManagerTree));
            File.WriteAllBytes(dataFilePath, data);
            Task.Run(() =>
                {
                    MessageBox.Show("Saved!");
                });
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(tbSearchFile.Text))
            {
                FileManagerTree tree = new FileManagerTree();
                var dir = new Node() { Name = "test", NodeType = NodeType.Directory };
                dir.Files["file.txt"] = new Node() { Name = "file.txt", NodeType = NodeType.File, Content = new byte[] { 38, 39, 3, 4, 5, 6 } };
                dir.Files["file2.txt"] = new Node() { Name = "file2.txt", NodeType = NodeType.File, Content = new byte[] { 38, 35, 34, 45, 51, 6 } };
                var subDir = new Node() { Name = "sub_dir", NodeType = NodeType.Directory };
                dir.Directories["sub_dir"] = subDir;
                tree.Root = dir;

                var s = FileManagerTree.Serialize(tree);
                AES aes = new AES();
                var c = aes.Encrypt(s);

                File.WriteAllBytes(dataFilePath, c);
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var node = fileManagerTreeView.SelectedNode;
            if (node != null)
            {
                var parentFolder = node.Parent.Tag as Node;
                var current = node.Tag as Node;
                if (current.NodeType == NodeType.Directory)
                {
                    parentFolder.Directories.Remove(current.Name);
                }
                else
                {
                    parentFolder.Files.Remove(current.Name);
                }
                node.Remove();
                fileManagerTreeView.SelectedNode = null;
            }
        }

        private void btnCreateFolder_Click(object sender, EventArgs e)
        {
            var node = fileManagerTreeView.SelectedNode;
            if (node != null)
            {
                var currentFolder = node.Tag as Node;
                if (currentFolder.NodeType == NodeType.Directory)
                {
                    Node newFolder = new Node()
                    {
                        NodeType = NodeType.Directory,
                        Name = "New Folder"
                    };
                    currentFolder.Directories[newFolder.Name] = newFolder;
                    TreeNode newNode = new TreeNode("New Folder", 0, 0)
                    {
                        Tag = newFolder
                    };
                    node.Nodes.Add(newNode);
                    node.Expand();
                    fileManagerTreeView.SelectedNode = newNode;
                    newNode.BeginEdit();
                }

            }
            else
            {
                Node newFolder = new Node()
                {
                    NodeType = NodeType.Directory,
                    Name = "New Folder"
                };
                fileManagerTree.Root.Directories[newFolder.Name] = newFolder;
                TreeNode newNode = new TreeNode("New Folder", 0, 0)
                {
                    Tag = newFolder
                };
                fileManagerTreeView.Nodes.Add(newNode);
                fileManagerTreeView.SelectedNode = newNode;
                newNode.BeginEdit();
            }
        }

        private void btnNewFile_Click(object sender, EventArgs e)
        {
            var node = fileManagerTreeView.SelectedNode;
            if (node != null)
            {
                var currentFolder = node.Tag as Node;
                if (currentFolder.NodeType == NodeType.Directory)
                {
                    Node newFolder = new Node()
                    {
                        NodeType = NodeType.File,
                        Name = "New File",
                        Content = new byte[1]
                    };
                    currentFolder.Files[newFolder.Name] = newFolder;
                    TreeNode newNode = new TreeNode("New File", 1, 1)
                    {
                        Tag = newFolder
                    };
                    node.Nodes.Add(newNode);
                    node.Expand();
                    fileManagerTreeView.SelectedNode = newNode;
                    newNode.BeginEdit();
                }

            }
            else
            {
                Node newFolder = new Node()
                {
                    NodeType = NodeType.File,
                    Name = "New File",
                    Content = new byte[1]
                };
                fileManagerTree.Root.Directories[newFolder.Name] = newFolder;
                TreeNode newNode = new TreeNode("New File", 1, 1)
                {
                    Tag = newFolder
                };
                fileManagerTreeView.Nodes.Add(newNode);
                fileManagerTreeView.SelectedNode = newNode;
                newNode.BeginEdit();
            }
        }

        private void fileManagerTreeView_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            var node = e.Node.Tag as Node;
            if (e.Label == null)
            {
                return;
            }
            if (e.Node.Parent != null)
            {
                var parentFolder = e.Node.Parent.Tag as Node;
                if (node.NodeType == NodeType.Directory)
                {
                    parentFolder.Directories.Remove(node.Name);
                    parentFolder.Directories[e.Label] = node;
                }
                else
                {
                    parentFolder.Files.Remove(node.Name);
                    parentFolder.Files[e.Label] = node;
                }
            }
            node.Name = e.Label;
        }

        private void fileManagerTreeView_AfterExpand(object sender, TreeViewEventArgs e)
        {
            var current = e.Node.Tag as Node;
            if (current.NodeType == NodeType.Directory)
            {
                foreach (TreeNode node in e.Node.Nodes)
                {
                    var child = node.Tag as Node;
                    if (node.Nodes.Count == 0 && child.NodeType == NodeType.Directory)
                    {
                        node.Nodes.AddRange(fileManagerTree.GetSubDirectoryTree(node.Tag as Node));
                    }
                }
            }
        }

        #endregion

        #region Drag&Drop

        private void treeViewItemDrag(object sender, ItemDragEventArgs e)
        {
            DoDragDrop(e.Item, DragDropEffects.Move);
        }

        private void treeViewDragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void treeViewDragDrop(object sender, DragEventArgs e)
        {
            TreeNode newNode;
            if (e.Data.GetDataPresent("System.Windows.Forms.TreeNode", false))
            {
                Point pt = ((TreeView)sender).PointToClient(new Point(e.X, e.Y));
                TreeNode destinationNode = ((TreeView)sender).GetNodeAt(pt);
                if (destinationNode.Tag is DirectoryInfo)
                {
                    newNode = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");
                    if (destinationNode.TreeView != newNode.TreeView)
                    {
                        //Modify Nodes Tags
                        CopyFromManagedFileToHdd(newNode, destinationNode);
                        //destinationNode.Nodes.Add((TreeNode)newNode.Clone());
                        destinationNode.Expand();
                    }
                    else
                    {
                        //Modify Nodes Tags
                        newNode.Remove();
                        destinationNode.Nodes.Add(newNode);
                        destinationNode.Expand();
                    }
                }
                else if (destinationNode.Tag is Node)
                {
                    var dest = destinationNode.Tag as Node;
                    if (dest.NodeType == NodeType.Directory)
                    {
                        newNode = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");
                        if (destinationNode.TreeView != newNode.TreeView)
                        {
                            //Modify Nodes Tags
                            CopyFromHddToManagedFile(newNode, destinationNode);
                            //destinationNode.Nodes.Add((TreeNode)newNode.Clone());
                            destinationNode.Expand();
                        }
                        else
                        {
                            //Modify Nodes Tags
                            var parentFolder = newNode.Parent.Tag as Node;
                            var destinationFolder = destinationNode.Tag as Node;
                            var current = newNode.Tag as Node;
                            if (current.NodeType == NodeType.Directory)
                            {
                                parentFolder.Directories.Remove(current.Name);
                                destinationFolder.Directories[current.Name] = current;
                            }
                            else
                            {
                                parentFolder.Files.Remove(current.Name);
                                destinationFolder.Files[current.Name] = current;
                            }
                            newNode.Remove();
                            destinationNode.Nodes.Add(newNode);
                            destinationNode.Expand();
                        }
                    }
                }
            }
        }

        private void CopyFromManagedFileToHdd(TreeNode newNode, TreeNode destinationNode)
        {
            var destinationFolder = destinationNode.Tag as DirectoryInfo;
            var copiedNode = newNode.Tag as Node;
            if (copiedNode.NodeType == NodeType.File)
            {
                File.WriteAllBytes(destinationFolder.FullName + "/" + copiedNode.Name, copiedNode.Content);
                var treeNode = new TreeNode(copiedNode.Name, 1, 1)
                {
                    Tag = new FileInfo(destinationFolder.FullName + "/" + copiedNode.Name)
                };
                destinationNode.Nodes.Add(treeNode);
            }
            else
            {

            }
        }

        private void CopyFromHddToManagedFile(TreeNode newNode, TreeNode destinationNode)
        {
            var destinationFolder = destinationNode.Tag as Node;
            if (newNode.Tag is FileInfo)
            {
                var fileInfo = newNode.Tag as FileInfo;
                var fileContent = File.ReadAllBytes(fileInfo.FullName);
                var newFile = new Node()
                {
                    Name = fileInfo.Name,
                    Content = fileContent,
                    NodeType = NodeType.File
                };
                destinationFolder.Files[newFile.Name] = newFile;
                var newFileNode = new TreeNode(newFile.Name, 1, 1)
                {
                    Tag = newFile
                };
                destinationNode.Nodes.Add(newFileNode);
            }
            else
            {
                var directoryInfo = newNode.Tag as DirectoryInfo;
                var directory = GetDirectoryFromHdd(directoryInfo);
                destinationFolder.Directories[directory.Name] = directory;
            }
        }

        private Node GetDirectoryFromHdd(DirectoryInfo root)
        {
            var rootDirectory = new Node()
            {
                Name = root.Name,
                NodeType = NodeType.Directory
            };

            foreach (var directory in root.GetDirectories())
            {
                rootDirectory.Directories[directory.Name] = GetDirectoryFromHdd(directory);
            }

            foreach (var file in root.GetFiles())
            {
                var newFile = new Node()
                {
                    Name = file.Name,
                    NodeType =  NodeType.File,
                    Content = File.ReadAllBytes(file.FullName)
                };
                rootDirectory.Files[newFile.Name] = newFile;
            }

            return rootDirectory;
        }

        #endregion        
    }
}
