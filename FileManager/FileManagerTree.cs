﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileManager
{
    public class FileManagerTree
    {
        public FileManagerTree()
        {
            Root = new Node();
        }

        public Node Root { get; set; }

        public static FileManagerTree Deserialize(string data)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<FileManagerTree>(data);
        }

        public static string Serialize(FileManagerTree tree)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(tree);
        }

        public TreeNode GetDirectoryTree(Node directoryInfo, int depth = 1)
        {
            var root = new TreeNode(directoryInfo.Name, 0, 0)
            {
                Tag = directoryInfo
            };

            if (depth > 0)
            {
                if (directoryInfo.Directories != null)
                {
                    foreach (var directory in directoryInfo.Directories)
                    {
                        root.Nodes.Add(GetDirectoryTree(directory.Value, depth - 1));
                    }
                }
                if (directoryInfo.Files != null)
                {
                    foreach (var file in directoryInfo.Files)
                    {
                        root.Nodes.Add(new TreeNode(file.Key, 1, 1) { Tag = file.Value });
                    }
                }
            }
            return root;            
        }

        public TreeNode[] GetSubDirectoryTree(Node directoryInfo, int depth = 1)
        {
            List<TreeNode> result = new List<TreeNode>();
            if (depth > 0)
            {
                if (directoryInfo.Directories != null)
                {
                    foreach (var directory in directoryInfo.Directories)
                    {
                        result.Add(GetDirectoryTree(directory.Value, depth));
                    }
                }
                if (directoryInfo.Files != null)
                {
                    foreach (var file in directoryInfo.Files)
                    {
                        result.Add(new TreeNode(file.Key, 1, 1) { Tag = file.Value });
                    }
                }
            }
            return result.ToArray();
        }
    }
}
